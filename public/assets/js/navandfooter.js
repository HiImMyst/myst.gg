var home = React.createElement(
    "a",
    { href: "index.html" },
    "Home"
);
var reactTest = React.createElement(
    "a",
    { href: "reactTest.html" },
    "React Test"
);
var contactMe = React.createElement(
    "a",
    { href: "contactMe.html" },
    "Contact"
);
var linkedIn = React.createElement(
    "a",
    { href: "https://www.linkedin.com/in/derrick-hamblin-36193b1b6/", target: "_blank" },
    "LinkedIn"
);
var gitLab = React.createElement(
    "a",
    { href: "https://gitlab.com/HiImMyst", target: "_blank" },
    "Gitlab"
);
var navBar = React.createElement(
    "h1",
    null,
    home,
    reactTest,
    contactMe
);
var footer = React.createElement(
    "h4",
    null,
    linkedIn,
    gitLab
);
ReactDOM.render(navBar, document.getElementById("navbar"));
ReactDOM.render(footer, document.getElementById("footer"));