var home = <a href="index.html">Home</a>;
var reactTest = <a href="reactTest.html">React Test</a>;
var contactMe = <a href="contactMe.html">Contact</a>;
var linkedIn = <a href="https://www.linkedin.com/in/derrick-hamblin-36193b1b6/" target="_blank">LinkedIn</a>;
var gitLab = <a href="https://gitlab.com/HiImMyst" target="_blank">Gitlab</a>;
var navBar = (
    <h1>
        { home }
        { reactTest }
        { contactMe }
    </h1>
);
var footer = (
    <h4>
        { linkedIn }
        { gitLab }
    </h4>
);
for (content, element in [[navBar, "navbar"], [footer, "footer"]]) {
    ReactDOM.render(
      content,
      document.getElementById(element)
    );
}